# Direct Connect Settings

resource "aws_dx_private_virtual_interface" "CBR-DEV-VIRTUAL-INTERFACE" {
  connection_id  = "${var.cbr-direct-connect-connection}"
  name           = "DEV-VIRTUAL-INTERFACE-CBR"
  vlan           = 4031
  address_family = "ipv4"
  bgp_asn        = 65000
  bgp_auth_key   = "${var.cbr-direct-connect-bgp-auth}"
  vpn_gateway_id = "${aws_vpn_gateway.DEV-VGW.id}"
}

resource "aws_dx_private_virtual_interface" "MEL-DEV-VIRTUAL-INTERFACE" {
  connection_id  = "${var.mel-direct-connect-connection}"
  name           = "DEV-VIRTUAL-INTERFACE-MEL"
  vlan           = 4031
  address_family = "ipv4"
  bgp_asn        = 65000
  bgp_auth_key   = "${var.mel-direct-connect-bgp-auth}"
  vpn_gateway_id = "${aws_vpn_gateway.DEV-VGW.id}"
}


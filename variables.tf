variable "aws_region" {
  default = "ap-southeast-2"
}

variable "dev_vpc_cidr" {
  default = "10.122.0.0/16"
}

variable "dev_subnets-a-public_cidr" {
  default = "10.122.0.0/24"
}

variable "dev_subnets-a-public_tag" {
  default = "DEV-A-DMZ-SERVERS"
}

variable "dev_subnets-b-public_cidr" {
  default = "10.122.10.0/24"
}

variable "dev_subnets-b-public_tag" {
  default = "DEV-B-DMZ-SERVERS"
}

variable "dev_subnets-c-public_cidr" {
  default = "10.122.20.0/24"
}

variable "dev_subnets-c-public_tag" {
  default = "DEV-C-DMZ-SERVERS"
}

# Private Subnets

variable "dev_subnets-a-private_cidr" {
  type = list(string)
  default = ["10.122.1.0/24", "10.122.2.0/24"]
}

variable "dev_subnets-b-private_cidr" {
  type = list(string)
  default = ["10.122.11.0/24", "10.122.12.0/24"]
}

variable "dev_subnets-c-private_cidr" {
  type = list(string)
  default = ["10.122.21.0/24", "10.122.22.0/24"]
}

variable "dev_subnets-a-private_tags" {
  type = list(string)
  default = ["DEV-A-APP-SERVERS", "DEV-A-DB-SERVERS"]
}

variable "dev_subnets-b-private_tags" {
  type = list(string)
  default = ["DEV-B-APP-SERVERS", "DEV-B-DB-SERVERS"]
}

variable "dev_subnets-c-private_tags" {
  type = list(string)
  default = ["DEV-C-APP-SERVERS", "DEV-C-DB-SERVERS"]
}

variable "availability_zone_1" {
  default = "ap-southeast-2a"
}

variable "availability_zone_2" {
  default = "ap-southeast-2b"
}

variable "availability_zone_3" {
  default = "ap-southeast-2c"
}

variable "open_cidr"{
  default = "0.0.0.0/0"
}

variable "open_cidr_ipv6" {
  default = "::/0"
}

variable "cbr_cidr"{
  default = "10.11.0.0/16"
}

variable "mel_cidr"{
  default = "10.2.0.0/16"
}

variable "cbr_vpn_cidr"{
  default = "172.16.0.0/16"
}

variable "mel_vpn_cidr"{
  default = "172.17.0.0/16"
}

variable "cbr-direct-connect-connection" {
    default = "dxcon-fhc6eppt"
}

variable "mel-direct-connect-connection" {
    default = "dxcon-fgi03nt9"
}

variable "cbr-direct-connect-bgp-auth" {
  	default = "0xTgD0pJeR5NBpdjks0xNZsr"
}

variable "mel-direct-connect-bgp-auth" {
  	default = "0xyiIV2gy20WUbahzmWiFyvd"
}

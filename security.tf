# SECURITY GROUPS

# DMZ SECURITY GROUP

resource "aws_security_group" "DEV-DMZ-SG" {
  name        = "DEV-DMZ-SG"
  description = "DMZ Security Group"
  vpc_id      =  "${aws_vpc.AMC-DEVELOPMENT.id}"

  ingress {
    description      = "DEV-DMZ-SG INGRESS TCP"
    from_port        = 443
    to_port          = 443
    protocol         = "tcp"
    cidr_blocks      = ["${var.open_cidr}"]
    ipv6_cidr_blocks = ["${var.open_cidr_ipv6}"]
  }
  ingress {
    description      = "DEV-DMZ-SG INGRESS TCP"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["${var.open_cidr}"]
    ipv6_cidr_blocks = ["${var.open_cidr_ipv6}"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["${var.open_cidr}"]
    ipv6_cidr_blocks = ["${var.open_cidr_ipv6}"]
  }

  tags = {
    Name = "DEV-DMZ-SG"
  }
}

# DEV APP SECURITY GROUP

resource "aws_security_group" "DEV-APP-SG" {
  name        = "DEV-APP-SG"
  description = "APP Security Group"
  vpc_id      =  "${aws_vpc.AMC-DEVELOPMENT.id}"

  ingress {
    description      = "DEV-APP-SG INGRESS TCP HTTPS"
    from_port        = 443
    to_port          = 443
    protocol         = "tcp"
    cidr_blocks      = ["${var.cbr_cidr}", "${var.mel_cidr}", "${var.cbr_vpn_cidr}", "${var.mel_vpn_cidr}"]
    security_groups  = ["${aws_security_group.DEV-DMZ-SG.id}"]
  }
  ingress {
    description      = "DEV-APP-SG INGRESS TCP HTTP"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["${var.cbr_cidr}", "${var.mel_cidr}", "${var.cbr_vpn_cidr}", "${var.mel_vpn_cidr}"]
    security_groups  = ["${aws_security_group.DEV-DMZ-SG.id}"]
  }
   ingress {
    description      = "DEV-APP-SG INGRESS TCP SSH"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["${var.cbr_cidr}", "${var.mel_cidr}", "${var.cbr_vpn_cidr}", "${var.mel_vpn_cidr}"]
    security_groups  = ["${aws_security_group.DEV-DMZ-SG.id}"]
  }
  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["${var.open_cidr}"]
    ipv6_cidr_blocks = ["${var.open_cidr_ipv6}"]
  }

  tags = {
    Name = "DEV-APP-SG"
  }
}


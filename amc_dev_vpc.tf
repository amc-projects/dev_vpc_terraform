# PROVIDER

provider "aws" {
  profile = "default"
  region  = "${var.aws_region}"
}

# VPC

resource "aws_vpc" "AMC-DEVELOPMENT" {
  cidr_block           = "${var.dev_vpc_cidr}"
  enable_dns_hostnames = "true"
  tags = {
    Name = "AMC-DEVELOPMENT"
  } 
}

# SUBNETS

resource "aws_subnet" "DEV-A-PUBLIC-SUBNET" {
  cidr_block        = "${var.dev_subnets-a-public_cidr}"
  vpc_id            = "${aws_vpc.AMC-DEVELOPMENT.id}"
  availability_zone = "${var.availability_zone_1}"
  tags = {
    Name = "${var.dev_subnets-a-public_tag}"
  }
}

resource "aws_subnet" "DEV-B-PUBLIC-SUBNET" {
  cidr_block        = "${var.dev_subnets-b-public_cidr}"
  vpc_id            = "${aws_vpc.AMC-DEVELOPMENT.id}"
  availability_zone = "${var.availability_zone_2}"
  tags = {
    Name = "${var.dev_subnets-b-public_tag}"
  }
}

resource "aws_subnet" "DEV-C-PUBLIC-SUBNET" {
  cidr_block        = "${var.dev_subnets-c-public_cidr}"
  vpc_id            = "${aws_vpc.AMC-DEVELOPMENT.id}"
  availability_zone = "${var.availability_zone_3}"
  tags = {
    Name = "${var.dev_subnets-c-public_tag}"
  }
}

resource "aws_subnet" "DEV-SUBNETS-A-PRIVATE" {
  count             = "${length(var.dev_subnets-a-private_cidr)}"
  vpc_id            = "${aws_vpc.AMC-DEVELOPMENT.id}"
  cidr_block        = "${element(var.dev_subnets-a-private_cidr,count.index)}"
  availability_zone = "${var.availability_zone_1}"
  tags = {
    Name = "${element(var.dev_subnets-a-private_tags,count.index)}"
  }
}

resource "aws_subnet" "DEV-SUBNETS-B-PRIVATE" {
  count             = "${length(var.dev_subnets-b-private_cidr)}"
  vpc_id            = "${aws_vpc.AMC-DEVELOPMENT.id}"
  cidr_block        = "${element(var.dev_subnets-b-private_cidr,count.index)}"
  availability_zone = "${var.availability_zone_2}"
  tags = {
    Name = "${element(var.dev_subnets-b-private_tags,count.index)}"
  }
}

resource "aws_subnet" "DEV-SUBNETS-C-PRIVATE" {
  count             = "${length(var.dev_subnets-c-private_cidr)}"
  vpc_id            = "${aws_vpc.AMC-DEVELOPMENT.id}"
  cidr_block        = "${element(var.dev_subnets-c-private_cidr,count.index)}"
  availability_zone = "${var.availability_zone_3}"
  tags = {
    Name = "${element(var.dev_subnets-c-private_tags,count.index)}"
  }
}

# INTERNET GATEWAY

resource "aws_internet_gateway" "DEV-IGW" {
  vpc_id = "${aws_vpc.AMC-DEVELOPMENT.id}"
  tags = {
    Name = "DEV-IGW"
  }
}

# VIRTUAL GATEWAY

resource "aws_vpn_gateway" "DEV-VGW" {
  vpc_id = "${aws_vpc.AMC-DEVELOPMENT.id}"
  tags = {
    Name = "DEV-DIRECT-CONNECT"
  }
}

# ELASTIC IP FOR NAT GATEWAY
resource "aws_eip" "DEV-NAT-EIP" {
  vpc = true
}

# NAT GATEWAY

resource "aws_nat_gateway" "NAT_GW" {
  allocation_id = "${aws_eip.DEV-NAT-EIP.id}"
  subnet_id     = "${aws_subnet.DEV-A-PUBLIC-SUBNET.id}"
  tags = {
    Name = "DEV-NAT-GW"
  }
}

# PUBLIC ROUTE TABLE 

resource "aws_route_table" "DEV-PUBLIC-ROUTE-TABLE" {
  vpc_id = "${aws_vpc.AMC-DEVELOPMENT.id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.DEV-IGW.id}"
 }
  tags = {
    Name = "DEV-PUBLIC-ROUTE-TABLE"
  }
}

# PUBLIC ROUTE TABLE ASSOCIATION 

resource "aws_route_table_association" "DEV-PUBLIC-ACCESS-A" {
  subnet_id      = "${aws_subnet.DEV-A-PUBLIC-SUBNET.id}"
  route_table_id = "${aws_route_table.DEV-PUBLIC-ROUTE-TABLE.id}"
}

resource "aws_route_table_association" "DEV-PUBLIC-ACCESS-B" {
  subnet_id      = "${aws_subnet.DEV-B-PUBLIC-SUBNET.id}"
  route_table_id = "${aws_route_table.DEV-PUBLIC-ROUTE-TABLE.id}"
}

resource "aws_route_table_association" "DEV-PUBLIC-ACCESS-C" {
  subnet_id      = "${aws_subnet.DEV-C-PUBLIC-SUBNET.id}"
  route_table_id = "${aws_route_table.DEV-PUBLIC-ROUTE-TABLE.id}"
}

# PRIVATE ROUTE TABLE

resource "aws_route_table" "DEV-PRIVATE-ROUTE-TABLE" {
  vpc_id = "${aws_vpc.AMC-DEVELOPMENT.id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_nat_gateway.NAT_GW.id}"
 }
  route {
    cidr_block = "10.11.0.0/16"
    gateway_id = "${aws_vpn_gateway.DEV-VGW.id}"
 }
 route {
    cidr_block = "10.2.0.0/16"
    gateway_id = "${aws_vpn_gateway.DEV-VGW.id}"
 }
 route {
    cidr_block = "172.16.0.0/16"
    gateway_id = "${aws_vpn_gateway.DEV-VGW.id}"
 }
 route {
    cidr_block = "172.17.0.0/16"
    gateway_id = "${aws_vpn_gateway.DEV-VGW.id}"
 }
  tags = {
    Name = "DEV-PRIVATE-ROUTE-TABLE"
  }
}

# PRIVATE ROUTE TABLE ASSOCIATIONS

resource "aws_route_table_association" "DEV-PRIVATE-ACCESS-A" {
  count          = "${length(var.dev_subnets-a-private_cidr)}"
  subnet_id      = "${aws_subnet.DEV-SUBNETS-A-PRIVATE.*.id[count.index]}"
  route_table_id = "${aws_route_table.DEV-PRIVATE-ROUTE-TABLE.id}"
}

resource "aws_route_table_association" "DEV-PRIVATE-ACCESS-B" {
  count          = "${length(var.dev_subnets-b-private_cidr)}"
  subnet_id      = "${aws_subnet.DEV-SUBNETS-B-PRIVATE.*.id[count.index]}"
  route_table_id = "${aws_route_table.DEV-PRIVATE-ROUTE-TABLE.id}"
}

resource "aws_route_table_association" "DEV-PRIVATE-ACCESS-C" {
  count          = "${length(var.dev_subnets-c-private_cidr)}"
  subnet_id      = "${aws_subnet.DEV-SUBNETS-C-PRIVATE.*.id[count.index]}"
  route_table_id = "${aws_route_table.DEV-PRIVATE-ROUTE-TABLE.id}"
}


